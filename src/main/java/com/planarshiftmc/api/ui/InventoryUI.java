package com.planarshiftmc.api.ui;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.stream.Collectors;

import com.planarshiftmc.api.ui.items.AbstractUIItem;
import com.planarshiftmc.api.ui.items.IQuestion;
import com.planarshiftmc.api.ui.items.UiItem;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.Event.Result;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;

/**
 * Creates a user clickable interface from an Inventory.
 * 
 * @author wispoffates
 */
public class InventoryUI extends AbstractUI implements Listener {

	protected int invSize;
	final protected Map<Integer, UiItem> indexedItems;
	final private Inventory inv;
	final protected boolean hasQuestions;
	final protected boolean acceptCancel;

	/**
	 * Create the Inventory UI.
	 * 
	 * @param plugin         The plugin creating the ui.
	 * @param name           The name of the inventory.
	 * @param items          The list of UI Items to use for the UI.
	 * @param user           The user this inventory is created for.
	 * @param fillItem       Optionally the item to fill empty space with.
	 * @param maxStackSize	 Optionally the max size of stack in the inventory.
	 * @param parent         Parent UIItem to return to when the menu completes.
	 * @param resultFunction The function that is called when the inventory is
	 *                       closed.
	 */
	public InventoryUI(Plugin plugin, String name, Collection<UiItem> items, Player user, Optional<ItemStack> fillItem,
			Optional<Integer> maxStackSize, boolean acceptCancel, Optional<AbstractUI> parent, Optional<UIResult> resultFunction) {
		super(plugin, name, items, user, parent, resultFunction);
		
		this.acceptCancel = acceptCancel;
		int numQuestions = items.size();
		int mod = numQuestions % 9;
		this.invSize = numQuestions / 9;
		this.invSize = this.invSize * 9;
		if (mod > 0) { // We overflow to the next line
			this.invSize += 9;
		}

		// indexing the questions the last row is centered for neatness
		this.indexedItems = new HashMap<>();
		int i = 0;
		for (UiItem item : items) {
			this.indexedItems.put(i, item);
			i++;
		}

		this.hasQuestions = this.indexedItems.values().stream().anyMatch((q) -> {
			return q instanceof IQuestion;
		});
		if (this.hasQuestions && this.acceptCancel) {
			this.invSize += 9; // Add a bottom row for the accept cancel buttons.
			this.indexedItems.put(this.invSize - 9, new CancelItem());
			this.indexedItems.put(this.invSize - 1, new AcceptItem());
		}

		this.inv = Bukkit.createInventory(null, this.invSize, name);
		maxStackSize.ifPresent((max) -> {this.inv.setMaxStackSize(max);});
	}

	// You can call this whenever you want to put the items in
	private void createItems() {
		// Add items
		for (Entry<Integer, UiItem> item : this.indexedItems.entrySet()) {
			this.inv.setItem(item.getKey(), item.getValue().createInventoryItem());
		}
	}

	@Override
	public void open() {
		//delay opening 3 ticks this lets other things like close get out of the way
		Bukkit.getScheduler().runTaskLater(this.plugin,()->{
			this.createItems();
			this.user.openInventory(this.inv);
			Bukkit.getServer().getPluginManager().registerEvents(this, this.plugin);
			return;
		},3);
	}

	/**
	 * Handles clicks on items in the inventory.
	 * 
	 * @param e The click event.
	 */
	@EventHandler
	public void onInventoryClick(InventoryClickEvent e) {
		// System.out.println("Click detected.");
		if (!(e.getWhoClicked() instanceof Player)) { // not a player so exit
			return;
		}

		Player p = (Player) e.getWhoClicked();
		if (!p.getUniqueId().equals(this.user.getUniqueId())) { // is it the correct user
			return;
		}

		if (!e.getInventory().equals(this.inv)) { // not our inventory so exit
			return;
		}

		//block double clicking as it doesnt trigger the slot the item is removed from
		if(e.getClick() == ClickType.DOUBLE_CLICK) {
			e.setCancelled(true);
		}

		int slot = e.getRawSlot();
		// check if we clicked a question
		if (this.indexedItems.containsKey(slot)) {
			UiItem item = this.indexedItems.get(slot);
			UISTATUS status = item.getAction().action(p, this, e);
			if (status == UISTATUS.UPDATE) {
				this.inv.setItem(slot, item.createInventoryItem());
			}
			if (status.shouldClose()) {
				this.close();
			}
			return;
		}

		// No moving things in and out of empty space in the gui
		if (e.getRawSlot() < this.invSize) {
			e.setCancelled(true);
			e.setResult(Result.DENY);
			return;
		}
		//this.debug(e);
	}

	/**
	 * Because sometimes a click is a drag...
	 */
	@EventHandler
	public void onDragEvent(InventoryDragEvent e) {
		// System.out.println("Click detected.");
		if (!(e.getWhoClicked() instanceof Player)) { // not a player so exit
			return;
		}

		Player p = (Player) e.getWhoClicked();
		if (!p.getUniqueId().equals(this.user.getUniqueId())) { // is it the correct user
			return;
		}

		if (!e.getInventory().equals(this.inv)) { // not our inventory so exit
			return;
		}
		for(Integer i : e.getRawSlots()) {
			if(i<this.invSize) {
				e.setCancelled(true);
				e.setResult(Result.DENY);
				return;
			}
		}
	}

	@SuppressWarnings("unused")
	private void debug(InventoryClickEvent e) {
		System.out.println("-----------------Clicked Debug----------------");
		System.out.println("Action: " + e.getAction());
		System.out.println("Slot: " + e.getSlot());
		System.out.println("Raw Slot: " + e.getRawSlot());
		System.out.println("Click Type: " + e.getClick());
		System.out.println("Current Item: " + e.getCurrentItem());
		System.out.println("Cursor Item: " + e.getCursor());
		System.out.println("Result: " + e.getResult());
		System.out.println("Inv Size: " + this.invSize);
	}

	@EventHandler
	public void onInventoryClose(InventoryCloseEvent e) {
		if (!(e.getPlayer() instanceof Player)) { // not a player so exit
			return;
		}

		Player p = (Player) e.getPlayer();
		if (!p.getUniqueId().equals(this.user.getUniqueId())) { // is it the correct user
			return;
		}

		if (!e.getInventory().equals(this.inv)) { // not our inventory so exit
			return;
		}
		this.close();
		if(!this.acceptCancel) {
			handleFinish(UISTATUS.COMPLETED);
		}
	}

	/**
	 * Accept button.
	 */
	protected class AcceptItem extends AbstractUIItem {

		public AcceptItem() {
			super("Accept", "Finish.");
			//Build the accept 
			this.setMaterial(ACCEPT);
		}
	
		@Override
		public UIAction getAction() {
			return (player, parent, event) -> {
				event.setCancelled(true);
				event.setResult(Result.DENY);
				List<String> unset = indexedItems.values().stream().filter(q -> q instanceof IQuestion)
						.filter(q -> !((IQuestion<?>) q).getAnswer().isPresent() && !((IQuestion<?>) q).isOptional())
						.map(q -> q.getName()).collect(Collectors.toList());
				if (!unset.isEmpty()) {
					player.sendMessage("Questions unanswered : " + String.join(",", unset));
					return UISTATUS.NO_UPDATE;
				}
				handleFinish(UISTATUS.COMPLETED);
				return UISTATUS.COMPLETED;
			};
		}
	}

	/**
	 * Cancel button.
	 */
	protected class CancelItem extends AbstractUIItem {

		public CancelItem() {
			super("Cancel", "Cancel.");
			this.setMaterial(CANCEL);
		}
	
		@Override
		public UIAction getAction() {
			return (player, parent, event) -> {
				event.setCancelled(true);
				event.setResult(Result.DENY);
				handleFinish(UISTATUS.CANCELED);
				return UISTATUS.CANCELED;
			};
		}
	}
	/**
	 * Pass current state to the resultHandler.
	 * @param status The reason for the exit.
	 */
	private void handleFinish(UISTATUS status) {
		if (resultHandler.isPresent()) {
			resultHandler.get().result(this.user, status, indexedItems.values().stream()
					.filter((q) -> q instanceof IQuestion)
					.map((q) -> {return (IQuestion<?>) q;})
					.collect(Collectors.toMap(IQuestion::getName, q -> q)));
		}
	}
}
