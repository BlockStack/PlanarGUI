package com.planarshiftmc.api.ui.items;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.planarshiftmc.api.ui.AbstractUI;

import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.TextComponent;
import net.kyori.adventure.text.format.NamedTextColor;

/**
 * Abstract class implements the basics of a UiItem.
 * 
 * @author wispoffates
 */
public abstract class AbstractUIItem implements UiItem {

	protected String name;
	protected String[] description;
	protected Optional<Material> material = Optional.empty();
	protected Optional<ItemStack> itemStack = Optional.empty();

	/**
	 * Constructor
	 * 
	 * @param name
	 *            The name of the element.
	 * @param description
	 *            The description of the element.
	 */
	protected AbstractUIItem(String name, String... description) {
		super();
		this.name = name;
		this.description = description;
	}

	/**
	 * Set the material to use for this element.
	 * 
	 * @param material
	 *            The material
	 */
	public void setMaterial(Material material) {
		this.material = Optional.ofNullable(material);
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public String[] getDescription() {
		return this.description;
	}

	@Override
	public Optional<Material> getMaterial() {
		return this.material;
	}

	@Override
	public Optional<ItemStack> getItemStack() {
		return this.itemStack;
	}

	/**
	 * Set an item stack for this element.
	 * 
	 * @param stack
	 *            The ItemStack to set.
	 */
	public void setItemStack(ItemStack stack) {
		this.itemStack = Optional.ofNullable(stack);
	}

	@Override
	public ItemStack createInventoryItem() {
		if (this.itemStack.isPresent()) {
			return this.itemStack.get();
		}

		Material mat = (this.material.isPresent()) ? this.getMaterial().get() : AbstractUI.SET_STATIC;
		ItemStack i = new ItemStack(mat, 1);
		ItemMeta iMeta = i.getItemMeta();
		iMeta.displayName(Component.text(this.name));
		List<Component> lore = new ArrayList<>();
		for(String desc : this.description) {
			lore.add(Component.text(desc).color(NamedTextColor.GREEN));
		}
		iMeta.lore(lore);
		i.setItemMeta(iMeta);
		return i;
	}

	@Override
	public TextComponent createTextComponent() {
		// create a page
		TextComponent text = Component.text(this.name);
		if(this.description.length > 0) {
			TextComponent desc = Component.text(this.description[0]);
			for(int i=1;i<this.description.length;i++) {
				desc.append(Component.text("\n"+this.description[i]));
			}
			text.hoverEvent(desc.asHoverEvent());
		}
		return text;
	}
}
