package com.planarshiftmc.api.ui.items;

import java.util.Optional;

/**
 * Represents a UI Item that is a question.
 * 
 * @param T The type of the answer.
 * @author wispoffates
 */
public interface IQuestion<T> extends UiItem {

	/**
	 * Retrieve the current answer to the question.
	 * 
	 * @return The current answer to the question or Optional.empty() if not
	 *         answered.
	 */
	public Optional<T> getAnswer();

	/**
	 * @return True if the player changed the value. False if the default value is being retured.
	 */
	public boolean hasChanged();

	/**
	 * Is this question optional to answer.
	 * 
	 * @return Is the question optional.
	 */
	public boolean isOptional();
}