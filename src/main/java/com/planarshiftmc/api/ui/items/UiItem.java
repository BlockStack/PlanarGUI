package com.planarshiftmc.api.ui.items;

import java.util.Optional;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import com.planarshiftmc.api.ui.UIAction;

import net.kyori.adventure.text.TextComponent;

/**
 * An element of the UI.
 * 
 * @author wispoffates
 */
public interface UiItem {
	/**
	 * The name of the element.
	 * 
	 * @return The name.
	 */
	public String getName();

	/**
	 * The description of the element.
	 * 
	 * @return The description.
	 */
	public String[] getDescription();

	/**
	 * Material to represent the item in the Inventory UI.
	 * 
	 * @return The material.
	 */
	public Optional<Material> getMaterial();

	/**
	 * Custom Item stack. Used for things like Player Heads.
	 * 
	 * @return The item stack.
	 */
	public Optional<ItemStack> getItemStack();

	/**
	 * Function to be performed once an element is clicked.
	 * 
	 * @return UIAction that performs the action.
	 */
	public UIAction getAction();

	/**
	 * Method to create the item stack used for the ui items.
	 * 
	 * @return The ItemStack.
	 */
	public ItemStack createInventoryItem();

	/**
	 * Method to create a TextComponent to be used in books or chat messages.
	 * 
	 * @return The TextComponent built from this element.
	 */
	public TextComponent createTextComponent();
}
