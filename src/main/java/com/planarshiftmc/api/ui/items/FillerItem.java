package com.planarshiftmc.api.ui.items;

import java.util.Arrays;
import java.util.Optional;

import com.planarshiftmc.api.ui.UIAction;
import com.planarshiftmc.api.ui.UISTATUS;

import org.bukkit.Material;

public class FillerItem extends AbstractUIItem {

    public FillerItem() {
        super("",Arrays.asList("").toArray(new String[0]));
        this.material = Optional.of(Material.BLACK_STAINED_GLASS_PANE);
    }

    @Override
    public UIAction getAction() {
        return (pl, parent, event) -> {
            if(event!=null) {
				event.setCancelled(true);
			}
            return UISTATUS.NO_UPDATE;
        };
    }

}