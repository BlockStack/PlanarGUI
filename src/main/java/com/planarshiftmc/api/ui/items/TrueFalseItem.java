package com.planarshiftmc.api.ui.items;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.planarshiftmc.api.ui.UIAction;
import com.planarshiftmc.api.ui.UISTATUS;

import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.format.NamedTextColor;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

/**
 * A short list item with two values true or false.
 * 
 * @author wispoffates
 */
public class TrueFalseItem extends AbstractUIItem implements IQuestion<Boolean> {

	private Boolean answer = false;
	private Boolean changed = false;

	/**
	 * @param name        Name of the item.
	 * @param description Description of the item.
	 * @param start       Starting value.
	 */
	public TrueFalseItem(String name, String[] description, Boolean start) {
		super(name, description);
		this.answer = start;
	}

	@Override
	public ItemStack createInventoryItem() {
		Material mat = this.answer.booleanValue() ? Material.GREEN_WOOL : Material.RED_WOOL;
		ItemStack i = new ItemStack(mat, 1);
		ItemMeta iMeta = i.getItemMeta();
		iMeta.displayName(Component.text(this.name));
		List<Component> lore = new ArrayList<>();
		for (String desc : this.description) {
			lore.add(Component.text(desc,NamedTextColor.GREEN));
		}
		lore.add(Component.text(""));
		lore.add(Component.text("Answer=" + this.answer,NamedTextColor.GREEN));
		lore.add(Component.text("[True,False]",NamedTextColor.GREEN));
		iMeta.lore(lore);
		i.setItemMeta(iMeta);
		return i;
	}

	@Override
	public UIAction getAction() {
		return (player, parent, event) -> {
			if(event!=null) {
				event.setCancelled(true);
			}
			this.answer = !this.answer;
			this.changed = true;
			return UISTATUS.UPDATE;
		};
	}

	@Override
	public Optional<Boolean> getAnswer() {
		return Optional.of(this.answer);
	}

	@Override
	public boolean isOptional() {
		return false;
	}

	@Override
	public boolean hasChanged() {
		return changed;
	}

}
