package com.planarshiftmc.api.ui.items;

import java.util.Optional;

import com.planarshiftmc.api.ui.UIAction;
import com.planarshiftmc.api.ui.UISTATUS;

import org.bukkit.Material;
import org.bukkit.event.Event.Result;
import org.bukkit.inventory.ItemStack;


/**
 * Question that expects an item stack to be placed into in an inventory.
 */
public class ItemStackQuestion extends AbstractUIItem implements IQuestion<ItemStack> {

    Optional<ItemStack> value = Optional.empty();
    private boolean changed = false;

    public ItemStackQuestion(String name, String[] description) {
        this(name,description,null);
    }

    public ItemStackQuestion(String name, String[] description, ItemStack current) {
        super(name,description);
        this.value = Optional.ofNullable(current);
    }

    @Override
    public UIAction getAction() {
        return (player, parent, e) -> {
            if(e==null) {
                //TODO: Handle book click somehow
                return UISTATUS.NO_UPDATE;
            }
            e.setCancelled(true);
            e.setResult(Result.DENY);
            ItemStack item = e.getCursor().clone();
            e.setCursor(this.value.orElse(new ItemStack(Material.AIR)));
            value = Optional.ofNullable(item);
            this.changed = true;
            //subtract one for the one we just put in the slot
            return UISTATUS.UPDATE;
        };
    }

    @Override
    public Optional<ItemStack> getAnswer() {
        return value;
    }

    @Override
    public boolean isOptional() {
        return true;
    }

    @Override
	public ItemStack createInventoryItem() {
        return this.value.orElse(new ItemStack(Material.AIR));
    }

    @Override
    public boolean hasChanged() {
        return changed;
    }

}