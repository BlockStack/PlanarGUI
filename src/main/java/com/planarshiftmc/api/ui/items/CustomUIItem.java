package com.planarshiftmc.api.ui.items;

import com.planarshiftmc.api.ui.UIAction;

/**
 * Completely custom UI element.
 * 
 * @author wispoffates
 */
public class CustomUIItem extends AbstractUIItem {

	protected UIAction action;

	/**
	 * Constructor
	 * 
	 * @param name
	 *            Name of the element.
	 * @param description
	 *            Description of the element.
	 */
	public CustomUIItem(String name, String[] description) {
		super(name, description);
	}

	/**
	 * Set the action to called when the element is clicked.
	 * 
	 * @param action
	 *            The action to be called.
	 */
	public void setAction(UIAction action) {
		this.action = action;
	}

	@Override
	public UIAction getAction() {
		return this.action;
	}
}
