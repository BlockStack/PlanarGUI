package com.planarshiftmc.api.ui.items;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.planarshiftmc.api.ui.UIAction;
import com.planarshiftmc.api.ui.UISTATUS;

import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.TextComponent;
import net.kyori.adventure.text.format.NamedTextColor;
import net.kyori.adventure.text.format.Style;
import net.kyori.adventure.text.format.TextDecoration;

/**
 * Inventory Question that does not allow its value to be changed. Most likely
 * used for information purposes.
 * 
 * @author wispoffates
 */
public class StaticValue extends AbstractUIItem implements IQuestion<String> {

	protected String value;

	/**
	 * Constructor
	 * 
	 * @param name
	 *            The name of the question.
	 * @param question
	 *            The question.
	 * @param value
	 *            The answer to the question.
	 */
	public StaticValue(String name, String[] question, String value) {
		super(name, question);
		this.value = value;
	}

	@Override
	public Optional<String> getAnswer() {
		return Optional.of(this.value);
	}

	@Override
	public boolean isOptional() {
		return false;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("StaticValue [name=").append(this.name).append(", question=").append(this.description)
				.append(", value=").append(this.value).append("]");
		return builder.toString();
	}

	// Do nothing since this is a static value with no action
	@Override
	public UIAction getAction() {
		return (p, ui, event) -> {
			if(event!=null) {
				event.setCancelled(true);
			}
			return UISTATUS.NO_UPDATE;
		};
	}
	
	@Override
	public ItemStack createInventoryItem() {
		Material mat = Material.ENCHANTED_BOOK;
		ItemStack i = new ItemStack(mat, 1);
		ItemMeta iMeta = i.getItemMeta();
		iMeta.displayName(Component.text(this.name));
		List<Component> lore = new ArrayList<>();
		for(String desc : this.description) {
			lore.add(Component.text(desc,NamedTextColor.GREEN));
		}
		lore.add(Component.text(""));
		lore.add(Component.text("Answer=" + this.value,NamedTextColor.GREEN));
		iMeta.lore(lore);
		i.setItemMeta(iMeta);
		return i;
	}

	@Override
	public TextComponent createTextComponent() {
		// create a page
		TextComponent text;
		if(this.value == null || this.value.isEmpty()) {
			text = Component.text(this.name);
		} else {
			text = Component.text(this.name + " : " + this.value);
		}
		text.style(Style.style(TextDecoration.BOLD,NamedTextColor.BLACK));
		if(this.description.length > 0) {
			Component hover = Component.text(this.description[0]);
			for(int i=1;i<this.description.length;i++) {
				hover.append(Component.text("\n"+this.description[i]));
			}
			text.hoverEvent(hover.asHoverEvent());
		}
		return text;
	}

	@Override
	public boolean hasChanged() {
		return false;
	}

}
