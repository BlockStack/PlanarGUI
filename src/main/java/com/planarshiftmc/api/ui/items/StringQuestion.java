package com.planarshiftmc.api.ui.items;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.planarshiftmc.api.ui.UIAction;
import com.planarshiftmc.api.ui.UISTATUS;

import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.TextComponent;
import net.kyori.adventure.text.format.NamedTextColor;
import net.wesjd.anvilgui.AnvilGUI;

/**
 * Standard text question getting an answer via anvil.
 * 
 * @author wispoffates
 */
public class StringQuestion extends AbstractUIItem implements IQuestion<String> {

	protected List<String> filtered = new LinkedList<>();
	protected Optional<String> answer = Optional.empty();
	protected Optional<Integer> maxLength = Optional.empty();
	protected boolean optional;
	protected boolean changed;

	/**
	 * Constructor
	 * 
	 * @param name
	 *            The name of question.
	 * @param question
	 *            The question.
	 * @param defaultValue
	 *            The default answer to the question.
	 * @param optional
	 *            Whether the question is option.
	 */
	public StringQuestion(String name, String[] question, Optional<String> defaultValue, boolean optional) {
		super(name, question);
		this.answer = defaultValue;
		this.optional = optional;
	}

	@Override
	public Optional<String> getAnswer() {
		return this.answer;
	}

	/**
	 * Set the answer to the question.
	 * 
	 * @param ans
	 *            The answer.
	 */
	public void setAnswer(String ans) {
		this.answer = Optional.ofNullable(ans);
	}

	/**
	 * @param maxLength the maxLength to set
	 */
	public void setMaxLength(Integer maxLength) {
		this.maxLength = Optional.ofNullable(maxLength);
	}

	/**
	 * Add filtered words to the question.
	 * @param filter The filtered words.
	 */
	public void setFilter(List<String> filter) {
		this.filtered = filter;
	}

	/**
	 * @return the maxLength
	 */
	public Optional<Integer> getMaxLength() {
		return maxLength;
	}

	@Override
	public boolean isOptional() {
		return this.optional;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("StringQuestion [answer=").append(this.answer).append(", optional=").append(this.optional)
				.append("]");
		return builder.toString();
	}

	@Override
	public UIAction getAction() {
		return (p, ui, event) -> {
			if(event!=null) {
				event.setCancelled(true);
			}
			ui.close();
			Bukkit.getScheduler().runTaskLater(ui.getPlugin(), ()-> {
				new AnvilGUI.Builder()
				.plugin(ui.getPlugin())
				.text(this.answer.orElse(this.name))
				.title(this.name)
				.onClick((slot,snapshot) -> {
					//ignore anything that isnt the output slot
					if(slot != AnvilGUI.Slot.OUTPUT) {
						return Collections.emptyList();
					}
					String reply = snapshot.getText();
					boolean valid = true;
					if(this.maxLength.isPresent() && reply.length() > this.maxLength.get()) {
						p.sendMessage("Value too long. The max length is " + this.maxLength.get());
						valid = false;
					}
					for(String filter : filtered) {
						if(reply.equalsIgnoreCase(filter)) {
							p.sendMessage("Invalid value.  This value is filtered or all ready exists. " + reply);
							valid = false;
						}
					}
					if(valid) {
						this.changed = true;
						setAnswer(reply);
					} else {
						reply="";
					}
					ui.open();
					return Arrays.asList(AnvilGUI.ResponseAction.close());
				})
				.open(p);
			}, 5);
			return UISTATUS.ASYNC;
		};
	}

	@Override
	public ItemStack createInventoryItem() {
		Material mat;
		if (this.answer.isPresent()) {
			mat = Material.BOOK;
		} else {
			mat = Material.PAPER;
		}
		ItemStack i = new ItemStack(mat, 1);
		ItemMeta iMeta = i.getItemMeta();
		iMeta.displayName(Component.text(this.name));
		List<Component> lore = new ArrayList<>();
		for(String desc : this.description) {
			lore.add(Component.text(desc,NamedTextColor.GREEN));
		}
		if (this.answer.isPresent()) {
			lore.add(Component.text(""));
			lore.add(Component.text("Answer=" + this.answer.get(),NamedTextColor.GREEN));
		}
		iMeta.lore(lore);
		i.setItemMeta(iMeta);
		return i;
	}
	
	@Override
	public TextComponent createTextComponent() {
		// create a page
		TextComponent text = Component.text(this.name);
		if (this.answer.isPresent()) {
			text = Component.text(this.name + " : " + this.answer.get());
		}
		if(this.description.length > 0) {
			Component hover = Component.text(this.description[0]);
			for(int i=1;i<this.description.length;i++) {
				hover.append(Component.text("\n"+this.description[i]));
			}
			text.hoverEvent(hover.asHoverEvent());
		}

		return text;
	}

	@Override
	public boolean hasChanged() {
		return this.changed;
	}

}
