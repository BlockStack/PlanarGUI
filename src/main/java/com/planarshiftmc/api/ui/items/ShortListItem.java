package com.planarshiftmc.api.ui.items;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.planarshiftmc.api.ui.UIAction;
import com.planarshiftmc.api.ui.UISTATUS;

import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.format.NamedTextColor;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

/**
 * A UI item representing a discrete set of values that are rotated between.
 * 
 * @author wispoffates
 */
public class ShortListItem extends AbstractUIItem implements IQuestion<String> {

	String[] values;
	String current;
	int curIndex = -1;
	protected boolean changed = false;

	/**
	 * Constructor
	 * 
	 * @param name
	 *                        Name of the toggle.
	 * @param description
	 *                        Description of the toggle.
	 * @param values
	 *                        Values for the toggle.
	 * @param current
	 *                        The item currently selected.
	 */
	public ShortListItem(String name, String[] description, String[] values, String current) {
		super(name, description);
		if (values == null || current == null) {
			throw new IllegalArgumentException("Neither values nor current can be null!");
		}
		this.current = current;
		this.values = values;
		for (int i = 0; i < values.length; i++) {
			if (current.equals(values[i])) {
				this.curIndex = i;
			}
		}
		if (this.curIndex == -1) {
			throw new IllegalArgumentException("current must equal a provided value!");
		}
	}

	@Override
	public UIAction getAction() {
		return (player, parent, event) -> {
			if(event!=null) {
				event.setCancelled(true);
			}
			this.curIndex++;
			if (this.curIndex >= this.values.length) {
				this.curIndex = 0;
			}
			this.current = this.values[this.curIndex];
			this.changed = true;
			return UISTATUS.UPDATE;
		};
	}

	@Override
	public Optional<String> getAnswer() {
		return Optional.of(this.current);
	}

	@Override
	public boolean isOptional() {
		return true;
	}

	@Override
	public ItemStack createInventoryItem() {
		Material mat = Material.ARROW;
		ItemStack i = new ItemStack(mat, 1);
		ItemMeta iMeta = i.getItemMeta();
		iMeta.displayName(Component.text(this.name));
		List<Component> lore = new ArrayList<>();
		for(String desc : this.description) {
			lore.add(Component.text(desc).color(NamedTextColor.GREEN));
		}
		lore.add(Component.text(""));
		lore.add(Component.text("Answer=" + this.current).color(NamedTextColor.GREEN));
		lore.add(Component.text("[" + String.join(",", this.values) + "]").color(NamedTextColor.GREEN));
		iMeta.lore(lore);
		i.setItemMeta(iMeta);
		return i;
	}

	@Override
	public boolean hasChanged() {
		return this.changed;
	}

}
