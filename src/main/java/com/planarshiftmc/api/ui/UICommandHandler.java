package com.planarshiftmc.api.ui;

import java.text.MessageFormat;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.function.BiConsumer;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

/**
 * The Command handler for UI command calls. Primarily used for handling book
 * commands.
 * 
 * @author wispoffates
 */
public class UICommandHandler implements CommandExecutor {
	private static final Map<String, UICommandHandler> handlers = new HashMap<>();

	private Map<NSCommandKey, BiConsumer<Player, String[]>> subCommands;
	private Plugin plugin;
	private String commandKey;

	protected UICommandHandler(String commandKey, Plugin plugin) {
		this.subCommands = new HashMap<>();
		this.commandKey = commandKey;
	}

	/**
	 * Get the handler for the provided plugin.
	 * 
	 * @param plugin
	 *                   Plugin to lookup.
	 * @return The UICommandHandler if its found or IllegalArguementException if it
	 *         is not.
	 */
	public static UICommandHandler getHandler(Plugin plugin) {
		UICommandHandler handler = handlers.get(plugin.getName());
		if (handler == null) {
			throw new IllegalArgumentException(plugin.getName() + " must call .addPlugin method first!");
		}
		return handler;
	}

	/**
	 * Get the Command Key for the provided plugin.
	 * 
	 * @param plugin
	 *                   Plugin to lookup.
	 * @return The Command Key if its found or IllegalArguementException if it is
	 *         not.
	 */
	public static String getHandlerKey(Plugin plugin) {
		UICommandHandler handler = handlers.get(plugin.getName());
		if (handler == null) {
			throw new IllegalArgumentException(plugin.getName() + " must call .addPlugin method first!");
		}
		return handler.getCommandKey();
	}

	public static UICommandHandler createEventHandler(Plugin plugin, String commandKey) {
		UICommandHandler handler = new UICommandHandler(commandKey, plugin);
		handler.plugin = plugin;
		handlers.put(plugin.getName(), handler);
		return handler;
	}


	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (args.length == 0) {
			return false;
		}
		if ("pDebug".equals(args[0])) {
			// debug commands
		}

		if ("pInternal".equals(args[0])) {
			try {
				NSCommandKey key = NSCommandKey.fromString(args[1]);
				if (this.subCommands.containsKey(key) && sender instanceof Player) {
					this.subCommands.get(key).accept((Player) sender, Arrays.copyOfRange(args, 1, args.length));
					return true;
				}
			} catch (IllegalArgumentException iae) {
				this.plugin.getLogger().severe(
						MessageFormat.format("Invalid internal command! :: $0 - $1 $2", label, args[0], args[1]));
			}
			return false;
		}
		return false;
	}

	/**
	 * Add a sub command called by the key and executes the command.
	 * 
	 * @param key
	 *                    The key for the command.
	 * @param command
	 *                    Function passed the Player calling the command and its
	 *                    arguments.
	 */
	public void addSubCommand(NSCommandKey key, BiConsumer<Player, String[]> command) {
		this.subCommands.put(key, command);
	}

	/**
	 * Remove the command by key.
	 * 
	 * @param key
	 *                The key of the command to remove.
	 */
	public void removeSubCommand(NSCommandKey key) {
		if (!key.key.isEmpty()) {
			this.subCommands.remove(key);
		} else {
			// handle namespace only removals.
			Iterator<Entry<NSCommandKey, BiConsumer<Player, String[]>>> it = this.subCommands.entrySet().iterator();
			while (it.hasNext()) {
				Entry<NSCommandKey, BiConsumer<Player, String[]>> ent = it.next();
				if (ent.getKey().namespace.equals(key.namespace)) {
					it.remove();
				}
			}
		}
	}

	/**
	 * @return The command key.
	 */
	public String getCommandKey() {
		return this.commandKey;
	}

	/**
	 * Namespaced key to allow deletes and narrowing by namespace.
	 * 
	 * @author wispoffates
	 */
	public static class NSCommandKey {
		/**
		 * The namespace of the key.
		 */
		public String namespace = "";
		/**
		 * The key.
		 */
		public String key = "";

		private NSCommandKey(String ns, String key) {
			this.namespace = ns.trim().replace(" ", "");
			this.key = key.trim().replace(" ", "");
		}

		/**
		 * Create key with both the namespace and key set..
		 * 
		 * @param ns
		 *                The namespace of the key.
		 * @param key
		 *                The key.
		 * @return A complete namespace key.
		 */
		public static NSCommandKey create(String ns, String key) {
			return new NSCommandKey(ns, key);
		}

		/**
		 * Create a partial key with only the namespace set.
		 * 
		 * @param ns
		 *               The namespace of the key.
		 * @return A partial namespace key.
		 */
		public static NSCommandKey create(String ns) {
			return new NSCommandKey(ns, "");
		}

		/**
		 * Parse an NSCommandKey from a string
		 * 
		 * @param input
		 *                  The string to parse.
		 * @return The NSCommandKey.
		 */
		public static NSCommandKey fromString(String input) {
			if (!input.contains("$$")) {
				throw new IllegalArgumentException("NSCommand key is not formatted correctly!  <namespace>$$<key>");
			}
			String[] parts = input.split("\\$\\$");
			if (parts.length != 2) {
				throw new IllegalArgumentException("NSCommand key is not formatted correctly!  <namespace>$$<key>");
			}
			return new NSCommandKey(parts[0], parts[1]);
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((this.key == null) ? 0 : this.key.hashCode());
			result = prime * result + ((this.namespace == null) ? 0 : this.namespace.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			NSCommandKey other = (NSCommandKey) obj;
			if (this.key == null) {
				if (other.key != null)
					return false;
			} else if (!this.key.equals(other.key))
				return false;
			if (this.namespace == null) {
				if (other.namespace != null)
					return false;
			} else if (!this.namespace.equals(other.namespace))
				return false;
			return true;
		}

		@Override
		public String toString() {
			return this.namespace + "$$" + this.key;
		}
	}

}
