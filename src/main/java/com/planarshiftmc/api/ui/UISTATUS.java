package com.planarshiftmc.api.ui;

/**
 * Potential return status of UI and UI Elements
 * 
 * @author wispoffates
 */
public enum UISTATUS {
	UPDATE, NO_UPDATE, CANCELED, COMPLETED, ASYNC;

	/**
	 * True if inventory should close or has closed.
	 * 
	 * @return True if UI should close.
	 */
	public boolean shouldClose() {
		return this == CANCELED || this == COMPLETED;
	}
}
