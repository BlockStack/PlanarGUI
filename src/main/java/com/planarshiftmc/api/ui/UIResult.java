package com.planarshiftmc.api.ui;

import java.util.Map;

import com.planarshiftmc.api.ui.items.IQuestion;

import org.bukkit.entity.Player;

/**
 * Functional interface for callback on UI Completion by an means.
 * 
 * @author wispoffates
 */
public interface UIResult {

	/**
	 * The callback for when a UI Completes.
	 * 
	 * @param p
	 *            Player that completed the UI.
	 * @param status
	 *            The reason the UI closed.
	 * @param answers
	 *            Map of questions and answers.
	 */
	public void result(Player p, UISTATUS status, Map<String,IQuestion<?>> answers);

}
