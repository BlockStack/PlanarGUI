package com.planarshiftmc.api.ui;

import java.util.Collection;
import java.util.Optional;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.plugin.Plugin;

import com.planarshiftmc.api.ui.items.UiItem;

/**
 * Abstract class to handle some of the basics of UI components.
 * 
 * @author wispoffates
 */
public abstract class AbstractUI implements Listener {

	public static final Material SET_CHANGEABLE = Material.BLUE_GLAZED_TERRACOTTA;
	public static final Material SET_STATIC = Material.PURPLE_GLAZED_TERRACOTTA;
	public static final Material NOT_SET_MANDATORY = Material.YELLOW_GLAZED_TERRACOTTA;
	public static final Material NOT_SET_OPTIONAL = Material.BROWN_GLAZED_TERRACOTTA;

	public static final Material ACCEPT = Material.EMERALD;
	public static final Material CANCEL = Material.REDSTONE;

	protected Collection<UiItem> uiItems;
	protected Optional<UIResult> resultHandler;
	protected Optional<AbstractUI> parent;
	protected Player user;
	protected String name;
	protected Plugin plugin;

	/**
	 * Create the UI.
	 * 
	 * @param plugin
	 *                           The plugin that creates the UI.
	 * @param name
	 *                           The name of the inventory.
	 * @param items
	 *                           The list of UI Items to use for the UI.
	 * @param user
	 *                           The user this inventory is created for.
	 * @param parent
	 *                           The parent UI Item to return to when this one
	 *                           closes.
	 * @param resultFunction
	 *                           The function that is called when the inventory is
	 *                           closed.
	 */
	public AbstractUI(Plugin plugin, String name, Collection<UiItem> items, Player user, Optional<AbstractUI> parent,
			Optional<UIResult> resultFunction) {
		this.plugin = plugin;
		this.name = name;
		this.uiItems = items;
		this.user = user;
		this.name = name;
		this.parent = parent;
		this.resultHandler = resultFunction;
	}

	/**
	 * Retrieve the items of the GUI element.
	 * 
	 * @return The list of items.
	 */
	public Collection<UiItem> getItems() {
		return this.uiItems;
	}

	public void close() {
		//new Throwable().printStackTrace();
		try {
			//delay 1 tick seems to fix the hotbar inventory update issue
			Bukkit.getScheduler().runTaskLater(this.plugin, ()->{
				HandlerList.unregisterAll(this);
				this.user.closeInventory();
			}, 1);
		} catch (Throwable t) {
			// Will throw annoying exceptions if the inventory was all ready open
		}
	}

	/**
	 * Open this UI element.
	 */
	public abstract void open();

	/**
	 * Retrieve the plugin that created the gui.
	 * 
	 * @return The plugin.
	 */
	public Plugin getPlugin() {
		return this.plugin;
	}

}
