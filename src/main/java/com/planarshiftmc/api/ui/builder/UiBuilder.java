package com.planarshiftmc.api.ui.builder;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

import com.planarshiftmc.api.ui.AbstractUI;
import com.planarshiftmc.api.ui.InventoryUI;
import com.planarshiftmc.api.ui.UIResult;
import com.planarshiftmc.api.ui.WrittenBookUI;
import com.planarshiftmc.api.ui.items.UiItem;

/**
 * Builder class to help with construction of a UI either book or inventory.
 * 
 * @author wispoffates
 */
public class UiBuilder {

	protected String name;
	protected List<UiItem> items;
	protected JavaPlugin plugin;
	protected Player player;
	protected boolean addAcceptCancel = true;
	protected Optional<ItemStack> fillStack = Optional.empty();
	protected Optional<UIResult> function;
	protected Optional<Integer> maxStackSize = Optional.empty();

	/**
	 * Constructor
	 * 
	 * @param name
	 *            The name for the UI
	 * @param plugin
	 *            The plugin controlling the UI. (For event monitoring)
	 * @param player
	 *            The player the UI is for.
	 */
	public UiBuilder(String name, JavaPlugin plugin, Player player) {
		super();
		this.name = name;
		this.plugin = plugin;
		this.player = player;
		this.items = new ArrayList<>();
	}

	/**
	 * Static Constructor
	 * 
	 * @param name
	 *            The name for the UI
	 * @param plugin
	 *            The plugin controlling the UI. (For event monitoring)
	 * @param player
	 *            The player the UI is for.
	 * @return a new instance.
	 */
	public static UiBuilder newInstance(String name, JavaPlugin plugin, Player player) {
		return new UiBuilder(name, plugin, player);
	}

	/**
	 * Add an inventory element.
	 * 
	 * @param item
	 *            The element to add.
	 * @return Chainable instance.
	 */
	public UiBuilder addInventoryElement(UiItem item) {
		this.items.add(item);
		return this;
	}

	/**
	 * Set a callback function which will be called when the UI completes. Player -
	 * The player calling the method. Map<S,S> - The results of any question
	 * elements.
	 * 
	 * @param func
	 *            The function to call.
	 * @return Chainable instance.
	 */
	public UiBuilder setCallback(UIResult func) {
		this.function = Optional.ofNullable(func);
		return this;
	}

	/**
	 * Fill the inventories empty space with the supplied item.
	 * @param item The item stack to act as the filler item.
	 * @return Chainable instance.
	 */
	public UiBuilder fillWithItem(ItemStack item) {
		this.fillStack = Optional.ofNullable(item);
		return this;
	}

	/**
	 * Never use this it just deletes the rest of the stack...
	 * Awesome job moang...
	 * So I've disabled it.
	 * @param maxsize The maxsize of a stack.
	 * @return Chainable instance.
	 */
	public UiBuilder setMaxStackSize(int maxsize) {
		//this.maxStackSize = Optional.of(maxsize);
		return this;
	}

	/**
	 * Whether to show the accept and cancel buttons if questions are present.
	 * @param val show accept and cancel buttons.
	 * @return Chainable instance.
	 */
	public UiBuilder setAddAcceptCancelButtons(boolean val) {
		this.addAcceptCancel = val;
		return this;
	}

	/**
	 * Build an Inventory UI.
	 * 
	 * @return The Inventory UI.
	 */
	public AbstractUI buildInventoryUI() {
		return new InventoryUI(this.plugin, this.name, this.items, this.player, this.fillStack, this.maxStackSize, this.addAcceptCancel, Optional.empty(), this.function);
	}

	/**
	 * Build an Inventory UI.
	 * 
	 * @return The Inventory UI.
	 */
	public AbstractUI buildBookUI() {
		return new WrittenBookUI(this.plugin, this.name, "Author", this.items, this.player, Optional.empty(),
				this.function);
	}

}
