package com.planarshiftmc.api.ui.builder;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import com.planarshiftmc.api.ui.UIAction;
import com.planarshiftmc.api.ui.items.AbstractUIItem;
import com.planarshiftmc.api.ui.items.CustomUIItem;
import com.planarshiftmc.api.ui.items.FillerItem;
import com.planarshiftmc.api.ui.items.ItemStackQuestion;
import com.planarshiftmc.api.ui.items.ShortListItem;
import com.planarshiftmc.api.ui.items.StaticValue;
import com.planarshiftmc.api.ui.items.StringQuestion;
import com.planarshiftmc.api.ui.items.TrueFalseItem;
import com.planarshiftmc.api.ui.items.UiItem;

/**
 * Chainable builder for UI Items
 * 
 * @author wispoffates
 */
public class UiItemBuilder {

	protected String name;
	protected String[] desc;
	protected boolean isQuestion = false;
	protected boolean isOptional = false;
	protected boolean isStatic = false;
	protected boolean isItemStack = false;
	protected boolean isFiller = false;
	protected Optional<String> defaultAnswer = Optional.empty();
	protected Optional<Material> material = Optional.empty();
	protected Optional<ItemStack> itemStack = Optional.empty();
	protected Optional<UIAction> action = Optional.empty();

	protected Optional<Integer> maxLength = Optional.empty();
	protected List<String> filtered = new LinkedList<>();
	protected Optional<Boolean> start = Optional.empty();
	protected Optional<String[]> values = Optional.empty();
	protected Optional<Boolean> listBookOrInv = Optional.empty();

	/**
	 * Constructor
	 * 
	 * @param name
	 *            Name of the element.
	 * @param description
	 *            Description of the element.
	 */
	public UiItemBuilder(String name, String... description) {
		if (name == null || description == null) {
			throw new IllegalArgumentException("Name and Description cannot be null!");
		}
		this.name = name;
		this.desc = description;
	}

	/**
	 * Static constructor
	 * 
	 * @param name
	 *            Name of the element.
	 * @param description
	 *            Description of the element.
	 * @return a new instance
	 */
	public static UiItemBuilder newInstance(String name, String... description) {
		return new UiItemBuilder(name, description);
	}

	public UiItemBuilder addDescriptionElement(String desc) {
		this.desc = Arrays.copyOf(this.desc, this.desc.length+1);
		this.desc[this.desc.length-1] = desc;
		return this;
	}

	/**
	 * Set the item as a question with a default answer.
	 * 
	 * @param defaultAnswer
	 *            The default to the question.
	 * 
	 * @return Chainable instance.
	 */
	public UiItemBuilder isQuestion(String defaultAnswer) {
		this.isQuestion = true;
		this.defaultAnswer = Optional.ofNullable(defaultAnswer);
		return this;
	}

	/**
	 * Set the item as a question without a default answer.
	 * 
	 * @return Chainable instance.
	 */
	public UiItemBuilder isQuestion() {
		this.isQuestion = true;
		this.defaultAnswer = Optional.empty();
		return this;
	}

	/**
	 * Set the item as an optional question.
	 * 
	 * @return Chainable instance.
	 */
	public UiItemBuilder isQptional() {
		this.isOptional = true;
		return this;
	}

	/**
	 * Set the item as an ItemStack
	 * @return Chainable instance.
	 */
	public UiItemBuilder isItemStack() {
		this.isItemStack = true;
		return this;
	}

	/**
	 * Set the item as filler.
	 * @return Chainable instance.
	 */
	public UiItemBuilder isFiller() {
		this.isFiller = true;
		return this;
	}


	/**
	 * Set the item as an optional question.
	 * 
	 * @param defaultAnswer
	 *                          The default answer.
	 * 
	 * @return Chainable instance.
	 */
	public UiItemBuilder isStatic(String defaultAnswer) {
		this.isStatic = true;
		this.defaultAnswer = Optional.ofNullable(defaultAnswer);
		return this;
	}

	/**
	 * Set the item as a true false question.
	 * 
	 * @param start
	 *                  The starting value.
	 * 
	 * @return Chainable instance.
	 */
	public UiItemBuilder isTrueFalse(boolean start) {
		this.start = Optional.of(start);
		return this;
	}

	/**
	 * Set the item as a short list question.
	 * 
	 * @param items
	 *                  The items of the list.
	 * @param start
	 *                  The starting value.
	 * 
	 * @return Chainable instance.
	 */
	public UiItemBuilder isShortList(String[] items, String start) {
		this.values = Optional.of(items);
		this.defaultAnswer = Optional.of(start);
		return this;
	}

	/**
	 * Set the questions default answer.
	 * 
	 * @param answer
	 *            The default answer.
	 * 
	 * @return Chainable instance.
	 */
	public UiItemBuilder setDefaultAnswer(String answer) {
		this.defaultAnswer = Optional.ofNullable(answer);
		return this;
	}

	/**
	 * Set a material to use for this item. Overrides the default.
	 * 
	 * @param material
	 *            The material to set.
	 * @return Chainable instance.
	 */
	public UiItemBuilder setMaterial(Material material) {
		this.material = Optional.ofNullable(material);
		return this;
	}

	/**
	 * Set the action to be called when the element is clicked.
	 * 
	 * @param action
	 *            The action to be called.
	 * @return Chainable instance.
	 */
	public UiItemBuilder setAction(UIAction action) {
		this.action = Optional.ofNullable(action);
		return this;
	}

	/**
	 * Set the item stack to represent the element.
	 * 
	 * @param stack
	 *            The item stack.
	 * @return Chainable instance.
	 */
	public UiItemBuilder setItemStack(ItemStack stack) {
		this.itemStack = Optional.ofNullable(stack);
		return this;
	}

	/**
	 * Set the max length of a string answer.
	 * @param length Max length.
	 */
	public UiItemBuilder setMaxLength(Integer length) {
		this.maxLength = Optional.of(length);
		return this;
	}

	/**
	 * Set a list of words that will be filtered.
	 * @param filter The list of filtered words.
	 */
	public UiItemBuilder setFilteredList(List<String> filter) {
		if(filter != null) {
			this.filtered = filter;
		}
		return this;
	}

	/**
	 * Build the UI Item.
	 * 
	 * @return The completed UI element.
	 */
	public UiItem build() {
		AbstractUIItem ret = null;

		// Build a question
		if (this.isQuestion) {
			ret = new StringQuestion(this.name, this.desc, this.defaultAnswer, this.isOptional);
			if(this.maxLength.isPresent()) {
				((StringQuestion)ret).setMaxLength(this.maxLength.get());
			}
			if(!this.filtered.isEmpty()) {
				((StringQuestion)ret).setFilter(this.filtered);
			}
		}

		// build true/false
		if (this.start.isPresent()) {
			ret = new TrueFalseItem(this.name, this.desc, this.start.get());
		}

		// Build short list
		if (this.values.isPresent()) {
			ret = new ShortListItem(this.name, this.desc, this.values.get(), this.defaultAnswer.get());
		}

		// Build static
		if (this.isStatic) {
			if (!this.defaultAnswer.isPresent()) {
				throw new IllegalArgumentException("Default value must be set on a static value.");
			}
			ret = new StaticValue(this.name, this.desc, this.defaultAnswer.get());
		}

		// Build Item Stack Question
		if(this.isItemStack) {
			ret = new ItemStackQuestion(this.name, this.desc, this.itemStack.orElse(null));
		}

		// Build Filler item
		if(this.isFiller) {
			ret = new FillerItem();
		}

		// build custom item since a custom action
		if (this.action.isPresent()) {
			ret = new CustomUIItem(this.name, this.desc);
			((CustomUIItem) ret).setAction(this.action.get());
		}


		if (this.material.isPresent()) {
			ret.setMaterial(this.material.get());
		}

		if (this.itemStack.isPresent()) {
			ret.setItemStack(this.itemStack.get());
		}

		return ret;
	}

}
