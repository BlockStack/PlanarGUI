package com.planarshiftmc.api.ui;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import com.planarshiftmc.api.ui.UICommandHandler.NSCommandKey;
import com.planarshiftmc.api.ui.items.UiItem;

import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.plugin.Plugin;

import net.kyori.adventure.inventory.Book;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.TextComponent;
import net.kyori.adventure.text.event.ClickEvent;

/**
 * A clickable book is the entry to a Book based UI.
 * 
 * @author wispoffates
 */
public class WrittenBookUI extends AbstractUI implements Listener {
	Book book;
	Player player;
	UUID id = UUID.randomUUID();

	/**
	 * Construct a clickable book.
	 * 
	 * @param plugin
	 *                   The plugin that is creating the UI.
	 * @param name
	 *                   Name of the book.
	 * @param author
	 *                   The Author of the book.
	 * @param items
	 *                   Items to be added to the book.
	 * @param user
	 *                   The player opening the book.
	 * @param parent
	 *                   Parent UI
	 * @param result
	 *                   Result handler function.
	 */
	public WrittenBookUI(Plugin plugin, String name, String author, Collection<UiItem> items, Player user,
			Optional<AbstractUI> parent,
			Optional<UIResult> result) {
		super(plugin, name, items, user, parent, result);
		this.player = user;

		Component bTitle = Component.text(name);
		Component bAuthor = Component.text(author);

		// create the book
		this.book = Book.book(bTitle, bAuthor, new ArrayList<>());
		this.updateBook();
	}
	
	@SuppressWarnings("unchecked") // Until java fixes generics and casting...
	private void updateBook() {
		// Create commands

		// add UI items
		// max of 12 so loop and add a new page
		int count = 0;
		Component page = null;
		List<Component> pages = new ArrayList<>();
		for (UiItem item : this.uiItems) {

			// setup command
			NSCommandKey cKey = NSCommandKey.create(this.id.toString(), item.getName());
			UICommandHandler.getHandler(this.plugin).addSubCommand(cKey, (player, args) -> {
				UISTATUS status = item.getAction().action(player, this, null);
				if (status.shouldClose()) {
					this.close();
				}
			});

			TextComponent text = item.createTextComponent();
			text.clickEvent(ClickEvent.runCommand(
					"/" + UICommandHandler.getHandlerKey(this.plugin) + " pInternal " + cKey.toString()));
			if (page == null) {
				page = text;
			} else {
				page.append(text);
			}
			if (count > 12) {
				pages.add(page);
				count=0;
				page = null;
			} else {
				//TODO: Fix later
				//page.a("\n");
			}
			count++;
		}
		if (page != null) {
			// TODO: Add Accept and Cancel
			pages.add(page);
		}
		this.book.pages(pages);
	}

	@Override
	public void open() {
		player.openBook(this.book);
		this.plugin.getServer().getPluginManager().registerEvents(this, this.plugin);
	}
	
	@Override
	public void close() {
		// nothing to close
	}
}
