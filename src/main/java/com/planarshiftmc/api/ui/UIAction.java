package com.planarshiftmc.api.ui;

import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;

/**
 * Callback similar to java predicate and function but has wrapper for reopening
 * the UI.
 * 
 * @author wispoffates
 */
public interface UIAction {

	/**
	 * @param p
	 *            Player initiating the action.
	 * @param parent
	 *            The parent UI element.
	 * @return Status of the UI element following the action.
	 */
	public abstract UISTATUS action(Player p, AbstractUI parent, InventoryClickEvent event);

}
